package com.kleber.facebookapp;

import java.net.URL;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.Friend;
import facebook4j.Paging;
import facebook4j.Reading;
import facebook4j.ResponseList;
import facebook4j.api.FriendMethods;

public class App 
{
    public static void main( String[] args ) throws FacebookException
    {
    	Facebook facebook = new FacebookFactory().getInstance();
    	//AccessToken at = new AccessToken("", null); //facebook.getOAuthAccessToken();
    	//facebook.setOAuthAccessToken(at);
    	//facebook.postStatusMessage("Testing Facebook4J.");
    	FriendMethods friends = facebook.friends();
    	ResponseList<Friend> results = friends.getFriends();
    	for( Friend friend: results) {
    		System.out.println(friend.getName());
    	}
    	
    	Paging<Friend> page = results.getPaging();
    	ResponseList<Friend> results2 = facebook.fetchNext(page);
    	for( Friend friend: results2) {
    		System.out.println(friend.getName());
    	}
    	


    }
}
